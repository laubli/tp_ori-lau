# TP de Laureline et Orianne

## Etapes de la pipeline

- **Etape 1 :** Build
- **Etape 2 :** Test
- **Etape 3 :** Docker Build (seulement avec utilisation des tags)

## Etape 1 : Build
Le build de l'application consiste à exécuter la commande `npm install` pour installer les dépendances de l'application. Le build est effectué à chaque commit.

## Etape 2 : Test
Après le build de l'application pour chaque commit, GitLab CI/CD exécute les tests définis dans le fichier .gitlab-ci.yml.

## Etape 3 : Docker Build
Lorsqu'un nouveau commit est poussé sur GitLab à chaque tag, GitLab CI/CD déclenche automatiquement la construction de l'image Docker définie dans le fichier .gitlab-ci.yml.
L'image Docker est construite à partir des sources de l'application.