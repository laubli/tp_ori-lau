# Utilisation d'une image de base (par exemple, une image avec le runtime Node.js)
FROM node:latest

# Définition du répertoire de travail dans le conteneur
WORKDIR /app

# Copie du code source de l'application dans le conteneur
COPY . .

# Installation des dépendances de l'application
RUN npm install

# Commande par défaut pour démarrer l'application
CMD ["npm", "start"]